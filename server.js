require('dotenv').config();
var express = require('express'),
app = express(),
port = 80,
mongoose = require('mongoose'),
playerModel = require('./api/models/player'),
bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_DB);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw({ extended: true }));

var routes = require('./api/routes/playerRoutes');
routes(app);

app.listen(port);

app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});