'use strict';

const jwt = require('./node_modules/jsonwebtoken');
const player = require('../models/player');

exports.authenticate = function(req, res, next) {
    try {
        const headerData = req.header('Authorization');
        if(!headerData) {
            throw new Error('No authorization provided. Expecting bearer token!');
        }
        var token = headerData.replace('Bearer ', '');

        //Perfect javascript implementations here
        token = token.replace('"', '');
        token = token.replace('"', '');
        console.log(token);
        const data = jwt.verify(token, 'pls');
        player.findOne({ name: data.name, 'tokens.token': token }, function(err, p){
            if(err) res.send(err);
            else {
                if(p)
                    req.playerID = p._id;
                else
                    throw new Error("Didn't find player");
                next();
            }
        });
    } catch (error) {
        res.status(401).send({ error: 'Not authorized to access this resource', message: error.message});
    }
};
