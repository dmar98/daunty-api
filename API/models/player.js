'use strict';

const mongoose = require('mongoose');
const schema = mongoose.Schema;
const crypto = require('crypto');
const jwt = require('./node_modules/jsonwebtoken');

var playerSchema = new schema({
    name: {
        type: String,
        index: {unique:true}
    },
    password: {
        type: String
    },
    salt: {
        type: String
    },
    tokens: [{
        token: {
            type: String
            }
        }],
    health: {
        type: Number
    },
    items: [{
        itemName: {
            type: String
        }
    }],
    map:{
        grid: [[{
            roomType:{
                type: Number
            },
            isPlayable:{
                type: Boolean
            }
        }
        ]]
    }
});

playerSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(32).toString('hex');
    this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

playerSchema.methods.validatePassword = function(password){
    var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.password === hash;
};

playerSchema.methods.generateAuthToken = function() {
    const player = this;
    const token = jwt.sign({name: player.name}, 'pls');
    player.tokens = player.tokens.concat({token});
    player.save();
    return token;
};

module.exports = mongoose.model('Player', playerSchema);