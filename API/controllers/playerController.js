'use strict';

var mongoose = require('mongoose'),
  Player = mongoose.model('Player');

exports.register = function(req, res){
    var new_player = new Player(req.body);
    new_player.setPassword(req.body.password);
    Player.findOne({name: req.body.name}, function(err, player) {
        if (err) {
            res.json(err);
        }
        if (player) {
            res.status(409).json("Player already exists!");
        }
        else {
            new_player.save(function(err, player) {
                if (err) {
                    res.json(err);
                }
                else {
                    var token = player.generateAuthToken();
                    res.json(token);
                }
            });
        }
    });
}

exports.login = function(req, res){
    Player.findOne({name: req.body.name}, function(err, player) {
        if (err) {
            res.json(err);
        }
        if (!player || !player.validatePassword(req.body.password)) {
            res.status(409).json("Wrong username or password!");
        }
        else {
            var token = player.generateAuthToken();
            res.json(token);
        }
    });
}

exports.getSaveData = function(req, res){
    Player.findOne({_id : req.playerID}, function(err, player){
        if (err) {
            res.json(err);
        } 
        if (!player){
            res.status(409).json("Didn't find player!");
        }
        else {
            res.json(player);
        }
    });
}

exports.updateSaveData = function(req, res){
    Player.findOne({_id : req.playerID}, function(err, player){
        if (err) {
            res.json(err);
        } 
        if (!player){
            res.status(409).json("Didn't find player!");
        }
        else {
            player.health = req.body.health;
            player.items = req.body.items;
            player.map = req.body.map;
            player.save();
            res.json("Everything saved.");
        }
    });
}

exports.allPlayers = function (req, res) {
    Player.find({}, function (err, result) {
        if (err) {
            res.json(err);
        } else {
            res.json(result);
        }
    });
}