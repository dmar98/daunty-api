'use strict';

module.exports = function(app){
    var playerController = require('../controllers/playerController');
    var authorization = require('../middleware/authorization');

    app.route('/players').get(playerController.allPlayers);

    app.route('/login').post(playerController.login);
    
    app.route('/register').post(playerController.register);

    app.route('/saveData').get(authorization.authenticate, playerController.getSaveData)
    .post(authorization.authenticate, playerController.updateSaveData);
}